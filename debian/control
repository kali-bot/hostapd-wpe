Source: hostapd-wpe
Section: net
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 12),
               pkg-config,
               libssl1.0-dev,
               libnl-3-dev,
               libnl-genl-3-dev,
               libsqlite3-dev
Standards-Version: 4.4.0
Homepage: https://github.com/aircrack-ng/aircrack-ng/tree/master/patches/wpe
Vcs-Git: https://gitlab.com/kalilinux/packages/hostapd-wpe.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/hostapd-wpe

Package: hostapd-wpe
Architecture: any
Depends: make-guile | make, ${shlibs:Depends}, ${misc:Depends}, openssl
Description: Modified hostapd to facilitate AP impersonation attacks
 This package contains hostapd modified with hostapd-wpe.patch.
 It implements IEEE 802.1x Authenticator and Authentication
 Server impersonation attacks to obtain client credentials,
 establish connectivity to the client, and launch other attacks
 where applicable. 
 .
 hostapd-wpe supports the following EAP types for impersonation:
    1. EAP-FAST/MSCHAPv2 (Phase 0)
    2. PEAP/MSCHAPv2
    3. EAP-TTLS/MSCHAPv2
    4. EAP-TTLS/MSCHAP
    5. EAP-TTLS/CHAP
    6. EAP-TTLS/PAP
 .
 Once impersonation is underway, hostapd-wpe will return an
 EAP-Success message so that the client believes they are connected
 to their legitimate authenticator. 
 .
 For 802.11 clients, hostapd-wpe also implements Karma-style gratuitous 
 probe responses. Inspiration for this was provided by JoMo-Kun's 
 patch for older versions of hostapd. 
 .
        http://www.foofus.net/?page_id=115
 .
 hostapd-wpe also implements CVE-2014-0160 (Heartbleed) attacks against
 vulnerable clients. Inspiration for this was provided by the Cupid PoC:
 .
        https://github.com/lgrangeia/cupid
 .
 hostapd-wpe logs all data to stdout and hostapd-wpe.log
